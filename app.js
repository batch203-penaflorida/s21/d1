let studentNumbers = [
  "2020-1923",
  "2020-1924",
  "2020-1925",
  "2020-1926",
  "2020-1927",
];

console.log(studentNumbers);
/* 
Arrays
   Array Literals
   -Arrays it also provides access to a number of functions/ methods that help in manipulation array.
      -Methods are used to manipulate information stored within the same object.
   - Arrays are used to store 
   - They are declared using square brackers ([]) also known as "Array Literals".
   -The main difference of arrays with object is that it contains information in a form of "list" unlike objects which uses "properties"
   (key-value pair).
   - Syntax:
      let/const arrayName = [elementA,elementB, elementC, ...]
   // Array for related elements
*/
let grades = [98.5, 94.3, 89.2, 90];
let computerBrands = ["Acer", "Toshiba", "Asus", "Fujitsu"];
console.log(grades);
console.log(computerBrands);

// Alternative way to write array

let myTasks = ["drink html", "eat javascript", "inhale css", "bake sass"];
console.log(myTasks);
// Creating an array with values from variables

let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];
console.log(cities);

// length property
/* 
   .length property
   .length property allows us to get and set the total number of items in an array.   
*/

console.log(myTasks.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);
let fullName = "Angelito Quiambao";
console.log(fullName.length);

//.length can also set the total number of items in an array.

myTasks.length = myTasks.length - 1;
console.log(myTasks.length);
console.log(myTasks);
//spaces are counted as characters
// To delete a specific item in an array we can employ array methods. We have only shown the logic or aglorithm of the "pop method".

//length property can also be used in string.

// Another example using decrement
cities.length--;
console.log(cities);

fullName.length = fullName.length - 1;
console.log(fullName.length);
console.log(fullName);

// We can also add the length of an array.

let theBeatles = ["John", "Paul", "Ringo", "George"];

theBeatles[theBeatles.length] = "Cardo";
console.log(theBeatles);

/* 
   Reading from Arrays
      - Accessing array elements is one of the common task that we do with an array.
      - This can be done through the use of array indexes.
      - Each element in an array is associated with it's own index number.
      - The first element in an array is associated with the number 0, and increasing
      this number by 1 for every element.
      - Array indexes it is actually refer to a memory address/location

      Array Address: 0x7ffe942bad0
      Array[0]=  0x7ffe942bad0
      Array[1] = 0x7ffe942bad4
      Array[2] = 0x7ffe942bad8
*/

console.log(grades[0]);
console.log(computerBrands[3]);
console.log(grades[20]);

let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];
console.log(lakersLegends[1]);
console.log(lakersLegends[3]);

let currentLaker = lakersLegends[2];
console.log(currentLaker);

console.log("Array before reassignment");
console.log(lakersLegends);

let bullsLegends = ["Jordan", "Pipen", "Rodman", "Rose", "Kukoc"];

let lastElemenetIndex = bullsLegends.length - 1;
console.log(bullsLegends[lastElemenetIndex]);

//adding items into the array

// using indices, we can add items

const newArr = [];
console.log(newArr[0]);
newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]);
newArr[1] = "Tifa Lockhart";
console.log(newArr);

//we can add items at the end of the array. using the array. length

newArr[newArr.length] = "Barret Wallace";
newArr[newArr.length - 1] = "Barret Wallace";
console.log(newArr);

// looping over an array
// You can use a for loop to iterate over all items in an array.
// forof
// foreach

for (let index = 0; index < newArr.length; index++) {
  // To be able to show each array items in the console.log

  console.log(newArr[index]);
}

// Create a program that will filter the array of numbers which are divisible by 5.

let numArr = [5, 12, 30, 46, 40, 52];

for (let i = 0; i < numArr.length - 1; i++) {
  if (numArr[i] % 5 === 0) {
    console.log(numArr[i] + " is divisble by 5.");
  } else {
    console.log(numArr[i] + " is not divisible by 5");
  }
}

// Multidimensional Arrays

/* 
   - Multidimensional arrays are useful for storing complex data structures.
   - A practical application of this is to help visualize/create real world objects.
   - This is frequently used to store for mathematic computations, image processing, and record management.
*/

// Create chessboard

let chessboard = [
  ["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
  ["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
  ["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
  ["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
  ["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
  ["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
  ["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
  ["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"],
];

console.table(chessboard);
console.log(chessboard[5][5]);
console.log(chessboard[1][5]);
